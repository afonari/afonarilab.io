<!-- subtitle: Anthracene-pyromellitic-dianhydride vibrations -->

### Anthracene-pyromellitic-dianhydride vibrations

 - <a href="/ant-pmda-vibrations/index-ir.html" target="_blank">IR vibrations <img src="/new_window.png" alt="" /></a>
 - <a href="/ant-pmda-vibrations/index-raman.html" target="_blank">Raman vibrations <img src="/new_window.png" alt="" /></a>
