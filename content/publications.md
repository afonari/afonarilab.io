<!-- subtitle: Publications -->

### Publications

Total: 23 peer-reviewed publications, cited 163 times, h-index is 7: [Google Scholar profile](http://scholar.google.com/citations?user=ZkBvorAAAAAJ)

1. **A. Fonari** *et al.*, &ldquo;Vibrational properties of organic donor-acceptor molecular crystals: Anthracene-pyromellitic-dianhydride (PMDA) as a case study&rdquo;, Journal of Chemical Physics (2015): [10.1063/1.4936965](http://doi.org/10.1063/1.4936965)

1. R. Castaneda, V. N. Khrustalev, **A. Fonari**, *et al.*, &ldquo;Mixed-Stack Architecture and Solvatomorphism of Trimeric Perfluoro-*ortho*-Phenylene Mercury complexes with Dithieno[3,2-*b*:2',3'-*d*]thiophene&rdquo;, Journal of Molecular Structure (2015): [10.1016/j.molstruc.2015.07.053](http://doi.org/10.1016/j.molstruc.2015.07.053)

1. A. A. Bakulin, R. Lovrincic, Y. Xi, O. Selig, H. J. Bakker, Y. L.A. Rezus, P. K. Nayak, **A. Fonari**, *et al.*, &ldquo;Mode-selective vibrational modulation of charge transport in organic electronic devices&rdquo;, Nature Communications (2015): [10.1038/ncomms8880](http://doi.org/10.1038/ncomms8880)

1. L. Stevens, K. Goetz, **A. Fonari**, *et al.*, &ldquo;Temperature Mediated Polymorphism in Molecular Crystals: The Impact on Crystal Packing and Charge Transport&rdquo;, Chemistry of Materials (2015): [10.1021/cm503439r](http://doi.org/10.1021/cm503439r)

1. K. Goetz, **A. Fonari**, *et al.*, &ldquo;Freezing-in Orientational Disorder Induces Crossover from Thermally Activated to Temperature-Independent Transport in Organic Semiconductors&rdquo;, Nature Communications (2014): [10.1038/ncomms6642](http://doi.org/10.1038/ncomms6642)

1. **A. Fonari** *et al.*, &ldquo;Impact of Exact Exchange in the Description of the Electronic Structure of Organic Charge-Transfer Molecular Crystals&rdquo;, Physical Review B (2014): [10.1103/PhysRevB.90.165205](http://doi.org/10.1103/PhysRevB.90.165205)

1. S. K. Mohapatra, **A. Fonari**, *et al.*, &ldquo;Dimers of Nineteen-Electron Sandwich Compounds: Crystal and Electronic Structures, and Comparison of Reducing Strengths&rdquo;, Chemistry -- A European Journal (2014): [10.1002/chem.201404007](http://doi.org/10.1002/chem.201404007). [Back cover](http://doi.org/10.1002/chem.201490197)

1. **A. Fonari** *et al.*, &ldquo;Toward Antikekulene: Angular 1,2-Di-, 2,3-Di-, and 1,2,15,16-Tetrachloro[6]phenylene&rdquo;, Synlett (2014): [10.1055/s-0034-1379140](http://doi.org/10.1055/s-0034-1379140)

1. L. Zhu, Y. Yi, **A. Fonari**, *et al.*, &ldquo;Electronic Properties of Mixed-Stack Organic Charge-Transfer Crystals&rdquo;, J. Phys. Chem. C (2014): [10.1021/jp502411u](http://doi.org/10.1021/jp502411u)

1. V. N. Khrustalev, B. Sandhu, S. Bentum, **A. Fonari**, *et al.*, &ldquo;Absolute Configuration and Polymorphism of 2-Phenylbutyramide and &alpha;-Methyl-&alpha;-phenylsuccinimide&rdquo;, Cryst. Growth Des. (2014): [10.1021/cg500284q](http://doi.org/10.1021/cg500284q)

1. L. Zhang, **A. Fonari**, *et al.*, &ldquo;Bistetracene: An Air-Stable, High-Mobility Organic Semiconductor with Extended Conjugation&rdquo;, Journal of the American Chemical Society (2014): [10.1021/ja503643s](http://doi.org/10.1021/ja503643s). [Front cover](http://pubs.acs.org/action/showLargeCover?jcode=jacsat&vol=136&issue=26)

1. L. Zhang, **A. Fonari**, *et al.*, &ldquo;Triisopropylsilylethynyl-Functionalized Graphene-Like Fragment Semiconductors: Synthesis, Crystal Packing, and Density Functional Theory Calculations&rdquo;, Chem. Eur. J. (2013): [10.1002/chem.201303308](http://doi.org/10.1002/chem.201303308). [Back cover](http://doi.org/10.1002/chem.201390209)

1. A. R. Morales, A. Frazer, A. W. Woodward, H.-Y. Ahn-White, **A. Fonari**, *et al.*, &ldquo;Design, Synthesis, and Structural and Spectroscopic Studies of Push-Pull Two-Photon Absorbing Chromophores with Acceptor Groups of Varying Strength&rdquo;, J. Org. Chem. (2013):[10.1021/jo302423p](http://doi.org/10.1021/jo302423p)

1. E. S. Leonova, N. S. Makarov, **A. Fonari**, *et al.*, &ldquo;Synthesis, structure, and one- and two-photon absorption properties of N-substituted 3,5-bisarylidene*propen*piperidin-4-ones&rdquo;, J. Mol. Struct. (2013): [10.1016/j.molstruc.2012.12.034](http://doi.org/10.1016/j.molstruc.2012.12.034)

1. A. O. El-Ballouli, R. S. Khnayzer, J. C. Khalife, **A. Fonari**, *et al.*, &ldquo;Diarylpyrenes *vs.* Diaryltetrahydropyrenes: Crystal Structures, Fluorescence, and Upconversion Photochemistry&rdquo;, J. Photochem. Photobiol. A (2013): [10.1016/j.jphotochem.2013.07.018](http://doi.org/10.1016/j.jphotochem.2013.07.018)

1. B. R. Kaafarani, A. O. El-Ballouli, R. Trattnig, **A. Fonari**, *et al.*, &ldquo;Bis(carbazolyl) Derivatives of Pyrene and Tetrahydropyrene: Synthesis, Structures, Optical Properties, Electrochemistry, and Electroluminescence&rdquo;, J. Mater. Chem. C (2013): [10.1039/c2tc00474g](http://doi.org/10.1039/c2tc00474g).

1. I. V. Magedov, N. M. Evdokimov, A. S. Peretti, M. Karki, D. T. Lima, L. Frolova, M. R. Reisenauer, A. E. Romero, P. Tongwa, **A. Fonari**, *et al.*, &ldquo;Reengineered Epipodophyllotoxin&rdquo;, Chem. Commun. (2012): [10.1039/c2cc35044k](http://doi.org/10.1039/c2cc35044k)

1. L. E. Polander, L. Pandey, A. Romanov, **A. Fonari**, *et al.*, &ldquo;2,6-Diacylnaphthalene-1,8:4,5-Bis(dicarboximides): Synthesis, Reduction Potentials, and Core Extension&rdquo;, J. Org. Chem. (2012): [10.1021/jo3006232](http://doi.org/10.1021/jo3006232)

1. I. V. Kosilkin, E. A. Hillenbrand, P. Tongwa, **A. Fonari**, *et al.*, &ldquo;Synthesis, Structure, Thermal and Nonlinear Optical Properties of a Series of Novel D-&pi;-A Chromophores with Varying Alkoxy Substituents&rdquo;, J. Mol. Struct. (2011): [10.1016/j.molstruc.2011.09.032](http://doi.org/10.1016/j.molstruc.2011.09.032)

1. **A. Fonari** *et al.*, &ldquo;Experimental and Theoretical Structural Study of (3E,5E)-3,5-bis-(benzylidene)-4-oxopiperidinium Mono- and (3E,5E)-3,5-bis-(4-N,N-dialkylammonio)benzylidene)-4-oxopiperidinium Trications&rdquo;, J. Mol. Struct. (2011): [10.1016/j.molstruc.2011.06.020](http://doi.org/10.1016/j.molstruc.2011.06.020)

1. **A. Fonari** *et al.*, &ldquo;Two Polymorphs of Phenanthro[4,5-*abc*]phenazine-18-crown-6: Preparation, X-ray Diffraction and DFT Studies&rdquo;, J. Mol. Struct. (2011): [10.1016/j.molstruc.2011.04.039](http://doi.org/10.1016/j.molstruc.2011.04.039)

1. **A. Fonari** *et al.*, &ldquo;On Justification of Cu(II) Environment in Mononuclear Complexes: Joint X-ray and AIM Studies&rdquo;, Polyhedron (2011): [10.1016/j.poly.2011.04.002](http://doi.org/10.1016/j.poly.2011.04.002)

1. E. S. Leonova, M. V. Makarov, E. Y. Rybalkina, S. L. Nayani, P. Tongwa, **A. Fonari**, *et al.*, &ldquo;Structure-Cytotoxicity Relationship in a Series of N-phosphorus Substituted *E,E*-3,5-bis(3-pyridinylmethylene)- and *E,E*-3,5-bis(4-pyridinylmethylene)piperid-4-ones&rdquo;, Eur. J. Med. Chem (2010): [10.1016/j.ejmech.2010.09.058](http://doi.org/10.1016/j.ejmech.2010.09.058)

### Oral Presentations

1. **A. Fonari** *et al.*, &ldquo;Joint Mean-Field and Ab Initio Study of the Exciton Dynamics Due to Low-Energy Phonons in Rubrene Single Crystal&rdquo;, Materials Research Society (MRS) Fall Meeting, Nov. 30 - Dec. 5, 2014, Boston, MA, USA

1. **A. Fonari** *et al.*, &ldquo;The Impact of Exact Exchange Energy in Describing the Charge-Transport Properties in Organic Charge-Transfer Semiconductors&rdquo;, International Conference on the Physics of Semiconductors (ICPS), Aug. 10-15, 2014, Austin, TX, USA

1. **A. Fonari** *et al.*, &ldquo;The Influence of the Hartree-Fock Exchange on the Charge-Transport Characteristics in Organic Crystalline Semiconductors&rdquo;, Southeast Theoretical Chemistry Association Meeting (SETCA), May 15-17, 2014, Atlanta, GA, USA

### Theses

1. **Ph.D.** Thesis: &ldquo;Theoretical Description of Charge-Transport and Charge-Generation Parameters in Single-Component and Bimolecular Charge-Transfer Organic Semiconductors&rdquo;, co-advisors: Veaceslav Coropceanu and Jean-Luc Bredas. [Thesis PDF](/FONARI-DISSERTATION-2015.pdf).

1. **M.Sc.** Thesis: &ldquo;X-Ray Diffraction and Computational Studies of Materials for Photonics and Electronic Applications&rdquo;, co-advisors: Mikhail Yu. Antipin and Tatiana V. Timofeeva

1. **B.Sc.** Thesis: &ldquo;Theoretical Investigations of Crossover Effects in Fe(II) Ions&rdquo;, advisor: Sophia I. Klokishner

