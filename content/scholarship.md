<!-- subtitle: Fonari STEM Scholarship -->

### Fonari Scholarship for Undergraduate STEM Students

The Fonari Scholarship offers a full tuition waiver for one semester at Terra Nova English Language Center ([www.terranovamd.com](http://www.terranovamd.com)) to Moldovan undergraduate students pursuing expertise in STEM fields.

This scholarship addresses the immense need that the Republic of Moldova has to produce skilled engineers and scientists and contributes to technological progress, scientific research, and international scientific collaboration.

Feel free to contact Terra Nova English Language Center at +37379581561 for support with your Fonari Scholarship application.

More details can be found [here](https://www.facebook.com/groups/toeflinmoldova/permalink/1297838683559837/).
