---
layout: default
title: Anthracene-pyromellitic-dianhydride Vibrations
---

## {{ page.title }}

 - [IR vibrations ![Will open in a new window](img/new_window.png)](http://afonari.com/ant-pmda-vibrations/index-ir.html){:target="_blank"}
 - [Raman vibrations ![Will open in a new window](img/new_window.png)](http://afonari.com/ant-pmda-vibrations/index-raman.html){:target="_blank"}
