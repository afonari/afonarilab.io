---
layout: default
title: Phonon eigenvectors in Quantum Espresso
---

## {{ page.title }}

Main reference \[1\], will expand on it later.

\[1\] a. http://qe-forge.org/pipermail/pw_forum/2006-August/079409.html; b. http://qe-forge.org/pipermail/pw_forum/2012-January/097411.html