---
layout: default
title: Alexandr Fonari
---

### Summary

![](/img/me.jpg)

 - I develop software to solve scientific problems in the field of materials science.
 - More than five years of experience with Perl, Python and Fortran languages.
 - Contributed to several quantum chemistry and condensed matter software packages:
    - NWChem (5M+ lines), Quantum Espresso (500K+ lines), *etc.*
 - 20+ peer-reviewed publications, cited 270+ times, h-index 9: [Google Scholar profile](https://scholar.google.com/citations?user=ZkBvorAAAAAJ).
 - Permanent resident of United States

### News
 - 19 Oct 2018: \#Venom :snake: :droplet: :skull: - pretty good, will watch again
 - 20 Sep 2017: Quantum ESPRESSO [integration](https://www.schrodinger.com/products/quantum-espresso) with Schr&ouml;dinger Materials Science Suite :muscle:
 - 13 Sep 2016: Deadline for [Fonari scholarship](/scholarship.html) is approaching, go grab it! :book:
 - 09 Apr 2016: Moved to Gitlab and added encryption!
 - 07 Jan 2016: Fonari Scholarship is **[up](/scholarship.html)**!
 - 15 Dec 2015: Paper on *Vibrational properties of organic donor-acceptor molecular crystals* is on the [JCP website](http://dx.doi.org/10.1063/1.4936965).
 - 08 Aug 2015: Joined [Schr&ouml;dinger](http://www.schrodinger.com/materials/)!
 - 29 Jul 2015: Got my PhD! [Thesis PDF](http://afonari.com/FONARI-DISSERTATION-2015.pdf).
 - 02 Jul 2015: Added list of publications (top menu &uarr;)
 - 05 Jun 2015: *Mode-selective vibrational modulation of charge transport* manuscript accepted to Nature Communications!

### Quote of the day

 - 17 Oct 2016: *openmpi is NOT OpenMP, the former is an open-source implementation of MPI, the later is shared-memory vectorization based on compile-time instructions.* -- [Lorenzo Paulatto at Pw_forum](http://qe-forge.org/pipermail/pw_forum/2017-October/114016.html) :computer:
 - 28 Jul 2016: Stay active :muscle: :bicyclist: :stuck_out_tongue_closed_eyes: *To say that you're exhausted is to telegraph that you're important, in demand, and successful.* -- [H. Rosefield at New Republic](https://newrepublic.com/article/135468/exhaustion-became-status-symbol)
 - 21 Jan 2016: *Never ever do a startup accelerator. EVER. Including YC, unless they take not more than 2.9% equity.* -- [Karthik Manimaran, Co-Founder at WeLink](http://www.karthikmanimaran.com/2016/01/21/why-we-applied-to-yc-despite-having-gone-through-another-accelerator/)
 - 02 Jan 2016: *Why place something on the heap if it can live on the stack?* -- [petke @ news.yc](https://news.ycombinator.com/item?id=10826635)
 - 17 Dec 2015: *We hire athletes and cross-train them.* -- [Marne Levine, Instagram COO](http://bloom.bg/1P7DkXA)
 - 16 Dec 2015: *Every program attempts to expand until it has an app store. Those programs which cannot so expand are replaced by ones which can.* -- [abrkn @ news.yc](https://news.ycombinator.com/item?id=10741954)